/*
     FN Module
     @summary Extends the V model with helpers
     @version 0.1
     @module fn
*/

V.mod.fn = {

  array: {

    /**
       @method V.mod.fn.array.looper
       @param {array} arr array to loop
       @author http://www.jstips.co/en/javascript/make-easy-loop-on-array/
       @example   
        var aList = ['A','B','C','D','E'];
        V.mod.fn.array.looper(aList);
     */
    looper(arr) {

      arr.loopIDX = 0;

      // return current item
      arr.current = function () {
        this.loopIDX = (this.loopIDX) % this.length; // no verification !!
        return arr[this.loopIDX];
      };

      // increment loopIDX AND return new current
      arr.next = function () {
        this.loopIDX++;
        return this.current();
      };

      // decrement loopIDX AND return new current
      arr.prev = function () {
        this.loopIDX += this.length - 1;
        return this.current();
      };
    },

    /**
       @method V.mod.fn.array.dedupe
       @description  remove duplicates from an array
       @returns {arr} deduped array
     */
    dedupe(arr) {
      //[1, 2, 3, 3] = > [1,2,3]
      const newArr = [...new Set(arr)];
      return newArr;
    },

    /**
      @method V.mod.fn.array.makeRegex
      @param  {array} array base array to transform
      @returns {regex}  formatted regex
    */
    makeRegex(array) {

      return new RegExp(array.join("|"), 'gi');
    },

    /**
        @method V.mod.fn.array.removeByVal
        @description Remove a value from an array by Value
        @param {array} arr Array of values to process
        @param {string} val Value to remove
        @returns {array} array of resulting values
        @example 
          V.mod.fn.array.removeByVal(["mon", "tue", "wed", "thur"], 'tue');
          result: ["mon", "wed", "thur"]
     */
    removeByVal(arr, val) {
      // iterate through the array and remove the value
      for (let i = 0; i < arr.length; i++) {
        if (arr[i] === val) {
          arr.splice(i, 1);
          break;
        }
      }
      // return result
      return arr;
    },

    /**
        @method V.mod.fn.array.removeByIndex
        @description Remove a value from an array by Index
        @param {array} arr Array of values to process
        @param {string} index index of value to remove
        @param {int} number  number to splice at
        @returns {array} array of resulting values
        @example  V.mod.fn.array.removeByIndex(array, index, 2);
    */
    removeByIndex(arr, index, number) {
      return arr.splice(index, number);
    },

    /**
        @method V.mod.fn.array.csvToArray
        @description Convert a csv formatted string into an array
        @param {string} str String to process
        @returns {array} Array of csv values
        @example V.mod.fn.array.csvToArray('a,b,c')
            returns: ['a', 'b', 'c']
     */
    csvToArray(str) {
      return str.split(",");
    },

    /** 
      @method V.mod.fn.array.arrayToCsv
      @description Convert an array into a csv formatted string
      @param {array} arr Array to process
      @returns {string} csv formatted string
      @example 
        V.mod.fn.array.arrayToCsv(['a', 'b', 'c'])
        returns: 'a,b,c'
    */
    arrayToCsv(arr) {
      return arr.valueOf();
    }
  },

  str: {

    /**
      @method V.mod.fn.str.shuffle
      @description Shuffle a string
      @param {string} str string to shuffle
      @example 
        V.mod.fn.str.shuffle('abcd')
        result: 'dacb'
    */
    shuffle(str) {
      const a = str.split("");
      const n = a.length;

      for (let i = n - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        const tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
      }
      return a.join("");
    },

    /**
      @method V.mod.fn.str.run
      @description Execute string values as functions
      @param {string} functionName function name in string
      @param {string} params params to pass
      @example V.mod.fn.str.run('alert', 'hello')
    */
    run(functionName, params) {
      const xfn = window[functionName];
      xfn(params);
    },

    /**
      @method V.mod.fn.str.stripTags
      @description Strips harmful tags from a string <>
      @param {string} str String to process
      @example 
        V.mod.fn.str.stripTags('<p>hello</p>')
        result: 'hello'
     */
    stripTags(str) {
      return str.replace(/(<([^>]+)>)/ig, "");
    },

    /**
      @method V.mod.fn.str.stripNumbers
      @description Strip numbers from a string
      @param {string} str String to process
      @example 
        V.mod.fn.str.stripNumbers('asd23')
        result: 'asd'
    */
    stripNumbers(str) {
      return str.replace(/[0-9]/g, '');
    },

    /**
      @method V.mod.fn.str.stripWhitespace
      @description Strip all whitespaces from a string
      @param {string} str String to process
      @example 
        V.mod.fn.str.stripWhitespace('string of data   ')
        result: 'stringofdata'
    */
    stripWhitespace(str) {
      return str.replace(/\s+/g, '');
    },

    /**
      @method V.mod.fn.str.stripWhitespaceOuter
      @description Strip Outer whitespaces from a string
      @param {string} str String to process
      @example 
        V.mod.fn.str.stripWhitespaceOuter('string of data   ')
        result: 'string of data'
     */
    stripWhitespaceOuter(str) {
      return str.replace(/(^\s+|\s+$)/g, '');
    },

    /**
      @method V.mod.fn.str.truncate
      @description Truncates text to a set word count
      @param {string} str text to process
      @param {int} number number of words to truncate after
      @returns {string} string result
      @example 
          V.mod.fn.str.truncate('a three word sentence', 3)
          result: 'a three word...'
     */
    truncate(str, number) {
      // split the string
      let words = str.split(' ');
      // splice by number
      words.splice(number, words.length - 1);
      // return result
      return words.join(' ') + (words.length !== str.split(' ').length ? '...' : '');
    },

    /**
      @method V.mod.fn.str.parseUsername
      @description Convert all '@name' mentions into active links
      @param {string} strstring value to parse
      @returns {string} processed string value
      @example 
          V.mod.fn.str.parseUsername('i am @me');
          result: "i am <a href="[document.location.host]#!/profile/me">@me</a>"
     */
    parseUsername(str) {
      // Finds all matching names with @   eg : @jimmy  and converts to link
      return str.replace(/[@]+[A-Za-z0-9-_]+/g, u => {
        const username = u.replace("@", "");
        //todo  decouple from site model dependencies for routing result
        return u.link(`${document.location.host}#!/profile/${username}`);
      });
    },

    /**
      @method V.mod.fn.str.parseHashtag
      @description Convert all '#tagname' mentions into active links
      @param {string} str string value to parse
      @returns {string} processed string value
      @example 
          V.mod.fn.str.parseHashtag('i am #me')
          result: "i am <a href="[document.location.host]#!/search/me">#me</a>"
     */
    parseHashtag(str) {

      // Finds all matching Hashtag names eg : #jimmy and converts to links
      return str.replace(/[#]+[A-Za-z0-9-_]+/g, param => {
        const tag = param.replace("#", "");
        //todo  decouple from site model dependencies for routing result
        return param.link(`${document.location.host}#!/search/${tag}`);
      });
    },

    /**
      @method V.mod.fn.str.capitalize
      @description Capitalize sentences
      @param {string} string string to process
      @returns {string} processed string
      @example 
          V.mod.fn.str.capitalize('i am jim')
          result: "I am jim"
     */
    capitalize(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    },

    center(str, spaces) {
      let space = '';
      let i = 0;
      while (i < spaces) {
        space += '&nbsp;';
        i++;
      }
      return space + str + space;
    },

    sentence(str) {
      return str.replace(/((?:\S[^\.\?\!]*)[\.\?\!]*)/g, txt => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());
    },

    append(str, suffix) {
      if (typeof str === 'string' && typeof suffix === 'string') {
        return str + suffix;
      }
      return str;
    },

    /**
       @method V.mod.fn.str.reverse
       @description Reverse a string
       @param {string} str String to reverse
       @returns {string} reversed string
     */
    reverse(str) {
      return str.split('').reverse().join('');
    },

    occurrences(str, substring) {
      const len = substring.length;
      let pos = 0;
      let n = 0;
      while ((pos = str.indexOf(substring, pos)) > -1) {
        n++;
        pos += len;
      }
      return n;
    },

    ellipsis(str, limit) {
      if (str.length <= limit) {
        return str;
      }
      return `${helpers.truncate(str, limit)}…`;
    }
  },

  obj: {

    /**
       @method V.mod.fn.obj.flattenTree
       @param  {object} ob object to flatten
       @returns {object} flattened object
     */
    flattenTree(ob) {
      const toReturn = {};
      for (const i in ob) {
        if (!ob.hasOwnProperty(i)) continue;
        if ((typeof ob[i]) == 'object') {
          const flatObject = V.mod.fn.obj.flattenTree(ob[i]);
          for (const x in flatObject) {
            if (!flatObject.hasOwnProperty(x)) continue;
            toReturn[`${i}.${x}`] = flatObject[x];
          }
        } else {
          toReturn[i] = ob[i];
        }
      }
      return toReturn;
    },

    /**
      @method V.mod.fn.obj.removeNulls
      @description Iterate over an object and remove null or empty values
      @param {json} JsonObj  JSON object to process
      @returns {json} processed Json Object
      @example V.mod.fn.obj.removeNulls(json-object)
    */
    removeNulls(JsonObj) {
      // iterate over an object and remove null or empty values.
      // this will also recursively run over value's properties as well so all use with care
      $.each(JsonObj, (key, value) => {
        if (key === null || value === "" || value === null) {
          delete JsonObj[key];
        } else if (typeof (value) === "object") {
          JsonObj[key] = V.mod.fn.obj.removeNulls(value);
        }
      });
      return JsonObj;
    },


    /**
      @method V.mod.fn.obj.searchObjects
      @description Search objects and return objects with a given key & value
      @param {object} obj object to process
      @param {string} key string key to process
      @param {string} val string value to process
      @returns {array} objects with given searchkey + val
      @example V.mod.fn.obj.searchObjects(obj, 'param', 'value')
     */
    searchObjects(obj, key, val) {
      let objects = [];
      for (i in obj) {
        if (typeof obj[i] === 'object') {
          objects = objects.concat(V.mod.fn.obj.searchObjects(obj[i], key, val));
        } else if (i === key && obj[key] === val) {
          objects.push(obj);
        }
      }

      return objects;
    }
  },

  num: {

    /**
      @method V.mod.fn.num.convert.bin
      @description Convert a number to binary
      @param {int} num number to convert
      @returns {string} Binary string result
      @example 
          V.mod.fn.num.convert.bin(123)
          result: "1111011"
     */
    bin(x) {
      // Convert a number to binary
      return x.toString(2);
    },

    /**
      @method V.mod.fn.num.convert.hex
      @description Convert a number to hex
      @param {int} num number to convert
      @returns {string} Hex string result
      @example 
          V.mod.fn.num.convert.hex(123)
          result: "7B"
     */
    hex(x) {
      return x.toString(16).toUpperCase();
    },

    /**
      @method V.mod.fn.num.convert.octal
      @description Convert a number to octal
      @param {int} num number to convert
      @returns {string} Octal string result
      @example 
          V.mod.fn.num.convert.octal(123)
          result: "173"
     */
    octal(num) {
      return num.toString(8);
    },

    /**
      @method V.mod.fn.num.rand
      @description Get a random number
      @returns {int} random number
      @example V.mod.fn.num.rand()
     */
    rand() {
      return (Math.random() * 1000000) + 1;
    },

    add(a, b) {
      return parseInt(a) + parseInt(b);
    },

    substract(a, b) {
      return parseInt(a) - parseInt(b);
    },

    divide(a, b) {
      return parseInt(a) / parseInt(b);
    },

    multiply(a, b) {
      return parseInt(a) * parseInt(b);
    },

    ceiling(num) {
      return Math.ceil(num);
    },

    floor(num) {
      return MAth.floor(parseInt(num))
    },

    phoneNumber(num) {
      num = num.toString();
      return `(${num.substr(0, 3)}) ${num.substr(3, 3)}-${num.substr(6, 4)}`;
    },

    shorten(num) {

      if (Math.abs(num) < 1000) {
        return num;
      }

      let shortNumber;
      let exponent;
      let size;
      let sign = num < 0 ? '-' : '';
      let suffixes = {
        'K': 6,
        'M': 9,
        'B': 12,
        'T': 16
      };

      num = Math.abs(num);
      size = num.toString().length;

      exponent = size % 3 === 0 ? size - 3 : size - (size % 3);
      shortNumber = Math.round(10 * (num / Math.pow(10, exponent))) / 10;
      
      for (const suffix in suffixes) {
        if (exponent < suffixes[suffix]) {
          shortNumber += suffix;
          break;
        }
      }

      return sign + shortNumber;
    }
  },

  getRandomColor() {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  },

  /**
      @method V.mod.fn.once
      @description Execute a function once then destroy it
      @param {function} fn
      @param {string} context
      @example
          var canOnlyFireOnce = V.mod.fn.once(function() {
              console.log('Fired!');
          });
          canOnlyFireOnce()
  */
  once(fn, context) {
    let result;
    return function () {
      if (fn) {
        result = fn.apply(context || this, arguments);
        fn = null;
      }
      return result;
    };
  },

  /**
     @method V.mod.fn.uuid
     @description Generate a Unique string for IDs
     @returns {string} unique UUID
   */
  uuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
      const r = Math.random() * 16 | 0;
      const v = c === 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  },

  html: {

    /**
       @method V.mod.fn.html.encode
       @description Converts a string to its html characters completely.
       @param {String} str String with unescaped HTML characters
     */
    encode(str) {
      const buf = [];
      for (let i = str.length - 1; i >= 0; i--) {
        buf.unshift(['&#', str[i].charCodeAt(), ';'].join(''));
      }
      return buf.join('');
    },

    /**
       @method V.mod.fn.html.decode
       @description Converts an html characterSet into its original character.
       @param {String} str htmlSet entities
     */
    decode(str) {
      return str.replace(/&#(\d+);/g, (match, dec) => String.fromCharCode(dec));
    }
  }
};
