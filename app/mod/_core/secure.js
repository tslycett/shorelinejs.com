/*
     @module secure
     @summary lite Browser encryption module

     NOTE: browser security doesn't exist!
     - secure the server, don't transmit sensitive data. 

     For proper transactional data, use strong encryption libs as these only provide obscurity
 */

V.mod.secure = {

  /**
    @method V.mod.secure.md5
    @description  MD5 encryption algorithm
    @param {string} str string to process
    @returns {string} md5 encrypted string
    @example
      V.mod.secure.md5('password')
  */
  md5(str) {
    var k = [],
      i = 0;
    for (; i < 64;) {
      k[i] = 0 | (Math.abs(Math.sin(++i)) * 4294967296);
    }
    function calcMD5(str) {
      var b, c, d, j,
        x = [];
      let str2 = unescape(encodeURI(str));
      let a = str2.length;
      let h = [b = 1732584193, c = -271733879, ~b, ~c];
      i = 0;
      for (; i <= a;) x[i >> 2] |= (str2.charCodeAt(i) || 128) << 8 * (i++ % 4);
      x[str = (a + 8 >> 6) * 16 + 14] = a * 8;
      i = 0;
      for (; i < str; i += 16) {
        a = h;
        j = 0;
        for (; j < 64;) {
          a = [d = a[3], ((b = a[1] | 0) + ((d = ((a[0] + [b & (c = a[2]) | ~b & d, d & b | ~d & c, b ^ c ^ d, c ^ (b | ~d)][a = j >> 4]) + (k[j] + (x[[j, 5 * j + 1, 3 * j + 5, 7 * j][a] % 16 + i] | 0)))) << (a = [7, 12, 17, 22, 5, 9, 14, 20, 4, 11, 16, 23, 6, 10, 15, 21][4 * a + j++ % 4]) | d >>> 32 - a)), b, c];
        }
        for (j = 4; j;) h[--j] = h[j] + a[j];
      }
      str = '';
      for (; j < 32;) str += ((h[j >> 3] >> ((1 ^ j++ & 7) * 4)) & 15).toString(16);
      return str;
    }
    return calcMD5(str);
  },

  // Unique shuffled key
  key: (V.mod.fn.str.shuffle('SXGWLZPDOKFIVUHJYTQBNMACERxswgzldpkoifuvjhtybqmncare')),

  /**
    @method V.mod.secure.encrypt
    @description Encypt a string
    @param {string} uncoded  raw / uncoded string
    @returns {string} encrpyted string
    @example 
        V.mod.secure.encrypt('hello')
        result: "jfIzD"
  */
  encrypt: function (uncoded, key) {
    (arguments[1]) ? '' : key = V.mod.secure.key;
    uncoded = uncoded.toUpperCase().replace(/^\s+|\s+$/g, "");
    var coded = "";
    var chr;
    for (var i = uncoded.length - 1; i >= 0; i--) {
      chr = uncoded.charCodeAt(i);
      coded += (chr >= 65 && chr <= 90) ?
        key.charAt(chr - 65 + 26 * Math.floor(Math.random() * 2)) :
        String.fromCharCode(chr);
    }
    return encodeURIComponent(coded);
  },

  /**
    @method V.mod.secure.decrypt
    @description Decrypt a string
    @param {string} coded
    @returns {string} decrpyted string
    @example V.mod.secure.decrypt('jfIzD')
        result: "hello"
  */
  decrypt: function (coded, key) {
    (key) ? '' : key = V.mod.secure.key;
    coded = decodeURIComponent(coded);
    var uncoded = "";
    var chr;
    for (var i = coded.length - 1; i >= 0; i--) {
      chr = coded.charAt(i);
      uncoded += (chr >= "a" && chr <= "z" || chr >= "A" && chr <= "Z") ?
        String.fromCharCode(65 + key.indexOf(chr) % 26) :
        chr;
    }
    return uncoded.toLowerCase();
  },

  /**
    @method V.mod.secure.hexEncode
    @description HEX encode a string
    @param {string} strVal String value to encode
    @returns {string} HEX encoded string
    @example 
        V.mod.secure.hexEncode('hello')
        result: "00680065006c006c006f"
  */
  hexEncode: function (strVal) {
    var hex, i;
    var result = "";
    for (i = 0; i < strVal.length; i++) {
      hex = strVal.charCodeAt(i).toString(16);
      result += ("000" + hex).slice(-4);
    }
    return result;
  },

  /**
    @method V.mod.secure.hexDecode
    @description HEX decode a string
    @param {string} strVal String value to decode
    @returns {string} HEX decoded string
    @example 
        V.mod.secure.hexDecode('00680065006c006c006f')
        result: "hello"
  */
  hexDecode: function (strVal) {
    var r = '';
    for (var i = 0; i < strVal.length; i += 2) {
      r += unescape('%' + strVal.substr(i, 2));
    }
    return r;
  }
};
