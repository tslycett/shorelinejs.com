/**
    @method V.event   
    @param {string} id id of element to add 'click' event listener
    @param {function} fn function to run when event is triggered
    @description Click events handler, uses element id.
    @example
    
    V.event('hello', function () { console.log('#hello clicked') });
    V.event['hello']()    - to execute
*/

// event constructor

V.event = (id, fn) => {
    (V.event[id]) ? V.log(`[V.event] :: #${id} :: was overwritten`) : V.event[id] = fn;
};

V.event._events = {};

/**
    @method V.event.on
    @description pubsub / events ON
    @param {string} eventName name of event to subscribe to
    @example V.event.on('hello', (data) => { V.log(data) })
 */
V.event.on = (eventName, fn) => {
    V.event._events[eventName] = V.event._events[eventName] || [];
    V.event._events[eventName].push(fn);
};

/**
    @method V.event.off
    @description pubsub / events OFF
    @param {string} eventName name of event to remove a function from
    @param {function} eventName name offunction to remove
    @example V.event.off('hello', fn)
 */
V.event.off = (eventName, fn) => {
    if (V.event._events[eventName]) {
        for (let i = 0; i < V.event._events[eventName].length; i++) {
            if (V.event._events[eventName][i] === fn) {
                V.event._events[eventName].splice(i, 1);
                break;
            }
        };
    }
};

/**
    @method V.event.emit
    @description pubsub / events - EMIT data to all events awaiting it
    @param {string} eventName name of event to subscribe to
    @example V.event.emit('hello',{hello: 'world'})
 */
V.event.emit = (eventName, data) => {
    if (V.event._events[eventName]) {
        V.event._events[eventName].forEach(fn => {
            fn(data);
        });
    }
};

//global listener
$(document).on("click", e => {
    V.event[e.target.id] ? V.event[e.target.id](e) : "";
});



// WIP - ignore
// V.event.parser = function (compiled) {
//     compiled = compiled.replace(/(onchange|onclick)="[^"]+"/gi, (v) => {
//       var uuid = V.mod.fn.uuid();
//       var event = v.replace(/onclick="|onchange="|"/gi, '');
//       V._state.events[uuid] = function (e) {
//         try { eval(event) } catch (err) { V.log(err) }
//       }
//       return `vid="${uuid}"`;
//     });
//     return compiled;
//   }

// // event listener
// V.event.listener = (el) =>{
//     $(el).on("click change",  (e) => {
//         if (V._state.events[e.target.id]) {
//             V._state.events[e.target.id](e)
//         }
//     });
// }