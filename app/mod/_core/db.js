/*
     @module V.db
     @summary in broswer DBs
 */

V.db = {
  
    _config: {
      cookies: false,
      session: false,
      local: false
    },
  
    cookie: {
  
      test() {
        V.db._config.cookies = true === (navigator.cookieEnabled ? true : false);
      },
  
      /**
          @method V.db.cookie.get
          @description Get a value from the cookie store
          @param {string} name    name of item to retrieve from cookie
           @returns {string} cookie value
          @example V.db.cookie.get('name-of-cookie-value');
       */
      get(name) {
        const start = document.cookie.indexOf(`${name}=`);
        const len = start + name.length + 1;
        if (!start && name !== document.cookie.substring(0, name.length)) {
          return null;
        }
        if (start === -1) {
          return null;
        }
        let end = document.cookie.indexOf(";", len);
        if (end === -1) end = document.cookie.length;
  
        return unescape(document.cookie.substring(len, end));
      },
  
      /**
          @method V.db.cookie.set
          @description Set a Cookie in browser
          @param {string} name    Name of the cookie
          @param {string} value   Value of the cookie
          @param {int} expires    Expiration date of the cookie (default: end of current session)
          @example  V.db.cookie.set('name', 'value', 65000, 'www.example.com', true);
       */
      set(name, value) {
        const d = new Date();
        d.setTime(d.getTime() + 14 * 24 * 60 * 60 * 1000);
        const expires = d.toUTCString();
        document.cookie = `${name}=${value};expires=${expires};`;
      },
  
      /**
          @method V.db.cookie.remove
          @description Remove an item from the cookie store by past dating the item for auto-removal
          @param {string} name
          @param {string} path
          @param {string} domain
          @example  
            V.db.cookie.remove('name', 'path', 'domain')
            simple remove: V.db.cookie.remove('name', '', '')
       */
      remove(name, path, domain) {
        if (V.db.cookie.get(name)) document.cookie = `${name}=${path ? `;path=${path}` : ""}${domain ? `;domain=${domain}` : ""};expires=Thu, 01-Jan-1825 00:00:01 GMT`;
      }
    },
  
  
    session: {
  
      /**
          @method V.db.session.set
          @description Store a value to browser session-storage
          @param {string} index name to index value by
          @param {string} value value to store
          @example V.db.session.set('index', 'value')
      */
      set(index, value) {
        // use sessionStorage for per tab storage
        window.sessionStorage.setItem(index, value);
      },
  
      /**
          @method V.db.session.get
          @description Get a value from browser session-storage
          @param {string} index index name of item to retrieve
          @example V.db.session.get('index name')
          @returns {string} retrieved storage item
       */
      get(index) {
        return window.sessionStorage.getItem(index);
      },
  
      /**
          @method V.db.session.remove
          @description Remove a value from browser session-storage
          @param {string} index index name of value to remove / delete
          @example V.db.session.remove('index name')
       */
      remove(index) {
        window.sessionStorage.removeItem(index);
      },
  
      /**
          @method V.db.session.clear
          @description Clear the Session-storage
          @example V.db.session.clear()
       */
      clear: function clear() {
        window.sessionStorage.clear();
      },
  
      polyfill() {
  
        // Modernizr version
        try {
          window.sessionStorage.setItem("x", "x");
          const testx = window.sessionStorage.getItem("x");
          return (testx === "x") ? true : false;
        } catch (e) {
          const storageNamespace = "sessionStoragePolyfill";
          const storage = document.createElement("div");
          if (typeof storage.addBehavior !== "undefined") {
            storage.id = "_storage";
            storage.style.display = "none";
            storage.style.behavior = "url(\"#default#userData\")";
            document.body.appendChild(storage);
            storage.load(storageNamespace);
            window.sessionStorage = {
              getItem: function getItem(id) {
                return storage.getAttribute(id) || undefined;
              },
              setItem: function setItem(id, val) {
                storage.setAttribute(id, val);
                storage.save(storageNamespace);
              },
              removeItem: function removeItem(id) {
                storage.removeAttribute(id);
                storage.save(storageNamespace);
              }
            };
          }
        }
      },
      test: function test() {
        try {
          sessionStorage.setItem("test", "worked");
          const result = sessionStorage.getItem("test");
          if (result === "worked") {
            // set config var
            V.db._config.session = true;
            //remove variable
            sessionStorage.removeItem("test");
            return true;
          } else {
            return false;
          }
        } catch (err) {
          V.log(`db.session.test : ${err}`);
          return false;
        }
      }
    },
  
  
    local: {
  
      /**
        @method V.db.local.set
        @description Store a value to the browser's local-storage (Retains data after refresh)
        @param {string} index
        @param {string} value
        @example V.db.local.set('index', 'value')
      */
      set(index, value) {
        const time = new Date();
        window.localStorage.setItem(`${index}-time`, time);
        window.localStorage.setItem(index, value);
      },
  
      /**
          @method V.db.local.get
          @description Get a value from the browser's local-storage
          @param {string} index
          @example V.db.local.get('index')
          @returns {string}
        */
      get(index) {
        return window.localStorage.getItem(index);
      },
  
      /**
        @method V.db.local.remove
        @description  Remove a value from the browser's local-storage
        @param {string} index
        @example V.db.local.remove('index')
      */
      remove(index) {
        window.localStorage.removeItem(index);
      },
  
      /**
        @method V.db.local.clear
        @description Clear all values from local-storage
        @example V.db.local.clear()
      */
      clear() {
        window.localStorage.clear();
      },
      polyfill() {
        // Modernizr version
        try {
          localStorage.setItem("x", "x");
          const testx = localStorage.getItem("x");
          return testx === "x" ? true : false;
        } catch (e) {
          // if localStorage isn't available but IE's userData API is, polyfill it - pattern based on https://gist.github.com/juliocesar/926500
          const storageNamespace = "localStoragePolyfill";
  
          const storage = document.createElement("div");
          if (typeof storage.addBehavior !== "undefined") {
            storage.id = "_storage";
            storage.style.display = "none";
            storage.style.behavior = "url(\"#default#userData\")";
            document.body.appendChild(storage);
  
            storage.load(storageNamespace);
  
            window.localStorage = {
              getItem: function getItem(id) {
                return storage.getAttribute(id) || undefined;
              },
              setItem: function setItem(id, val) {
                storage.setAttribute(id, val);
                storage.save(storageNamespace);
              },
              removeItem: function removeItem(id) {
                storage.removeAttribute(id);
                storage.save(storageNamespace);
              },
              clear: function clear() {
                return false;
              } // no way to appropriately polyfill this
            };
          }
        }
      },
  
      test() {
        try {
          localStorage.setItem("test", "worked");
          const result = localStorage.getItem("test");
          if (result === "worked") {
            // set config var
            V.db._config.local = true;
            //remove variable
            localStorage.removeItem("test");
            return true;
          } else {
            return false;
          }
        } catch (err) { 
          V.log(`db.local.test${err}`);
          return false;
        }
      }
    },
  
    init() {
      try {
        V.db.cookie.test();
        V.db.session.test();
        V.db.local.test();
      } catch (err) { V.log(err) }
    }
  };
  
  V.db.init();