/*
    BROWSER module
    @module V.mod.browser
    @summary Browser info & polyfills
    @version 0.2
    @author github/timlycett
 */

V.mod.browser = {

  width: window.innerWidth,
  height: window.innerHeight,
  type: (window.innerWidth > 769) ? 'desktop' : 'mobile',
  
  w3c: document.implementation.hasFeature("org.w3c.svg", "1.0") || false,
  appCodeName: navigator.appCodeName || false,
  appName: navigator.appName || false,
  appVersion: navigator.appVersion || false,
  userAgent: navigator.userAgent || false,
  platform: navigator.platform || false,
  online: navigator.onLine || false,
  serviceWorker: navigator.serviceWorker || false,
  doNotTrack: navigator.doNotTrack || false,
  languages: navigator.languages || false,
  product: navigator.product || false,
  hardwareConcurrency: navigator.hardwareConcurrency || false,
  maxTouchPoints: navigator.maxTouchPoints || false,
  vendorSub: navigator.vendorSub || false,
  vendor: navigator.vendor || false,
  productSub: navigator.productSub || false,
  cookieEnabled: navigator.cookieEnabled || false,

  init() {
    // keep window size updated
    window.onresize = function () {
      V.mod.browser.width = window.innerWidth;
      V.mod.browser.height = window.innerHeight;
      V.mod.browser.type = (window.innerWidth > 768) ? 'desktop' : 'mobile';
    };
  },
};
