
/**
	 @module V.data
	 @description Global data storage and onchange event listener  (document bubble level)
   @param {string} el selector to monitor
   @param {string} namespace what namespace to store the data on
	 @example 		
   	V.data('#form', 'myform')
  	> All changes will now be stored under the elements id on V.data.myform
 */
V.data = (el, namespace) => {

	// create our custom storage space
	V.data[namespace] = {};

	// listen to editable divs
	$(`${el}[contentEditable]`).on("blur keyup paste copy cut mouseup", e => { V.data[e.target.id] = e.target.textContent });

	// create a focused form change event listener
	$(el).on("change", e => {
		// remove hashvalues
		let x = e.target.id;
		// strip hashtags if present
		x = x.replace("#", "");
		// assign value
		V.data[namespace][x] = e.target.value;
		// event handler
		(V.data._change[x]) ? V.data._change[x]() : '';
		// block event propogation to document level
		e.stopPropagation();
	});
};

// autolog all changes data model
$(document).on("change", e => {
	V.data._change[e.target.id] = e.target.value;
});

// set namespace
V.data._change = {};
