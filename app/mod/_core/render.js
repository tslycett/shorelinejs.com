/**
   @method V.render
   @desc Render engine
   @param  {object} obj  renderer
   @example

    V.render({
      el: 'body',
      data: {a:'b'},
      template: `<h1>{{a}}</h1>`
    })

   @example

    V.render({
      api: '/api',
      template: '{{#each this}}<h1>{{this}}</h1>{{/each}}',
      callback() { }
    })

  @example 

    // full list of options
    V.render({
      el: '',             // The $() selector to use, or else <body> is the default
      layout: '',         // optional layout to use for the render
      template: '',       // can be compiled templates, dom templates or JS strings with handlebars power ;)
      api: '',            // Api is linked to the REQ mod, and will request the data before rendering ($.ajax / socket.io)
      filter(){},         // filter data received from API before rendering   (useful for api above)
      data: {},           // Data object to use for rendering when not using api
      prerender(){},      // Compile template and manipulate rendered string before it is rendered to the dom
      callback(){}        // The callback for tasks after rendering
    });
 */
V.render = obj => {

  let options = {

    // default element to render to
    el: V._config.element || "body",

    // prerender layout and render to it
    layout: V._config.layout || false,

    // template to use
    template: false,
    // run a pre-rendering function on conmpiled string
    prerender: false,

    // url to GET data from
    api: false,
    // filter data
    filter: false,

    // data object to use
    data: false,

    // after render do
    callback: false
  };
  $.extend(options, obj, true);


  // there is no api request and data is defined
  if (options.api === false && options.data !== false) {

    // render layout if applicable
    if (options.layout !== false) {
      document.body.innerHTML = V.render.compile(options.layout, {})
    }

    // compile with Template7 engine
    let compiled = Template7.compile(options.template);

    // render data into template if applicable
    if (options.data !== false) {
      compiled = compiled(options.data)
    }

    // Allow manipulation on content before dom insertion
    if (options.prerender !== false) {
      compiled = options.prerender(compiled)
    }

    // send rendered string to dom
    $(options.el).html(compiled);

    // exec callback
    if (options.callback !== false) {
      options.callback()
    }

  } else {

    V.req({
      url: options.api,
      data: options.data,
      success(urlData) {

        // render layout if applicable
        if (options.layout !== false) {
          document.body.innerHTML = V.render.compile(options.layout, {})
        }

        // do filtering if set
        options.filter !== false
          ? urlData = options.filter(urlData)
          : "";

        // render template via engine
        let compiled = Template7.compile(options.template);
        compiled = compiled(urlData);

        // Allow manipulation on content before dom insertion
        if (options.prerender !== false) {
          compiled = options.prerender(compiled)
        }

        // send rendered string to dom
        $(options.el).html(compiled);

        // exec callback
        if (options.callback !== false) {
          options.callback(urlData)
        }
      }
    });
  }
};

/**
   @method V.render.compile
   @param  {string} template
   @param  {object} data
   @returns {string}
   @example
      V.render.compile('{{a}}', {a: 'b'});
      returns: "b"
*/
V.render.compile = (template, data) => {
  const compiled = Template7.compile(template);
  return compiled(data);
}

//REGISTER HELPERS

/**
 * @member _link
 * @description Template helper for links
 * @prop {string} url url to link to
 * @prop {string} title title of link
 * @example {{_link "url" "title" class=""}}
 * @returns <a href="url" class="class" target="_blank" title="title">title</a>
 */
Template7.registerHelper('_link', (url, title, options) => `<a href="${url}" class="${options.hash.class}" target="_blank" title="${title}">${title}</a>`);

/**
 * @member _num
 * @description Template helper for V.mod.fn.num
 * @prop {string} functionName date helper's name to use
 * @prop {string} value date string to process
 * @example {{_num "functionName" "value"}}   - same as V.mod.fn.num["functionName"](value)
 */
Template7.registerHelper('_num', (selector, value, opt) => {
  opt = opt || false;
  return V.mod.fn.num[selector](value, opt);
});

/**
 * @member _str
 * @description Template helper for V.mod.fn.str
 * @prop {string} functionName date helper's name to use
 * @prop {string} value date string to process
 * @example {{_str "functionName" "value"}}   - same as V.mod.fn.str["functionName"]("value")
 */
Template7.registerHelper('_str', (selector, value) => V.mod.fn.str[selector](value.toString()));

/**
 * @member _date
 * @description Template helper for V.mod.fn.date
 * @prop {string} functionName date helper's name to use
 * @prop {string} value date string to process
 * @example {{_date "functionName" "value"}}   - same as V.mod.fn.date["functionName"]("value")
 */
Template7.registerHelper('_date', (selector, value) => V.mod.date[selector](value.toString()));

/**
 * @member _com
 * @description Template helper for V.com   - finds template and model to render
 * @prop {string} name Component's name to use
 * @prop {string} props to override component's default model with
 * @example {{_com "nav" prop="value"}}
 */
Template7.registerHelper('_com', (name, props) => {
  if (V.com[name] && (V.com[name].template || V.templates && V.templates[`components/${name}/${name}.html`])) {

    const model = {};
    if (V.com[name].model) { $.extend(model, V.com[name].model, true) };
    $.extend(model, props.hash, true);

    const template = V.com[name].template || V.templates[`components/${name}/${name}.html`] || 'a template is required for components';
    return V.render.compile(template, model);

  } else {
    V.log(`{{_com "${name}"}} :: components requires a model and template`);
  }
});

// set globals
Template7.global = V._config.site;