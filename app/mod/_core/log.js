/**
	@module V.log
	@description Built in controlled console.log() replacement
	@version 0.1
	@example V.log('hello')
*/

V.log = function(msg) {
	if (document.location.href.match(/localhost/)) { console.log(msg) }
	V.log.book.push(msg);
	// TIP: add your logging api for production error feedback

};

// create our log book
V.log.book = [];

// Detect and report all window errors
window.onerror = (msg, url, l) => {
	V.log(msg);
	if (V.debug) { V.debug.trace(l) };
};

/**
 * @method V.log.silent
 * @description silence the console output
 * @example 
 * 	if(document.location.href.match('example.com')) { V.log.silent() }
 */
V.log.silent = () => {
	console.log = (e) => { V.log(e) }
	console.warn = (e) => { V.log(e) }
	console.error = (e) => { V.log(e) }
	console.trace = (e) => { V.log(e) }
}