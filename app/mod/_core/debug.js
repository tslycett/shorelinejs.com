/*
	DEBUG helpers
*/
V.debug = {

	id() {

		// check for duplicate ids
		const ids = {};
		let found = false;
		$('[id]').each(function () {
			if (this.id && ids[this.id]) {
				found = true;
				console.warn(`Duplicate ID #${this.id}`);
			}
			ids[this.id] = 1;
		});
	},

	write_object(obj) {
		// Write a js memory object to dom
		$('body').append(`<code>${JSON.stringify(obj, null, 4)}</code>`);
	},

	trycatch(object) {
		// wraps all functions in an object into a try catch for debugging
		let name;

		let method;
		for (name in object) {
			method = object[name];
			if (typeof method == "function") {
				object[name] = ((name, method) => function () {
					try {
						V.log(`${name}exec`);
						return method.apply(this, arguments);

					} catch (ex) {
						V.warn(`ERROR :: ${name}() : ${ex.message}`);
					}
				})(name, method);
			}
		}
	},

	trace(variable) {
		console.trace(variable);
	},

	printr(o) {
		if (o) {
			// conversion of objects into strings
			const z = JSON.stringify(o, null, '\t').replace(/\n/g, '<br>').replace(
				/\t/g, ' ');
			V.log(`🔊${z}`);
			return x;
		}
	},

	/**
	  @method V.debug.objSize
		@description  Validate the existence of each key in the object to get the number of valid keys
		@param {object} the_object
		@example    V.debug.objSize(object-var)
	 */
	objSize(the_object) {

		// function to validate the existence of each key in the object to get the number of valid keys
		let object_size = 0;
		for (const key in the_object) {
			if (the_object.hasOwnProperty(key)) {
				object_size++;
			}
		}
		V.log(`object size  ${object_size}`);
		return object_size;
	},

	/**
	  @method V.debug.editable
		@description make the document editable
		@example V.debug.editable()
	 */
	editable() {
		document.body.contentEditable = 'true';
		document.designMode = 'on';
		void 0;
		V.log('all content editable');
	},
};
