/*
     @module V.mod.element
     @version 0.1
 */

V.mod.element = {

  /**
      @method V.mod.element.cursorPosition
      @description Get the cursor's position in a textarea / input
      @param {string} element the id / class /.. selector
      @returns {integar} position of cursor
      @example V.mod.element.cursorPosition('#id')
   */
  cursorPosition(element) {
    // get the element from the DOM
    const el = $(element).get(0);
    // set a default position of 0
    let pos = 0;
    // IF cursor is at a position
    if ('selectionStart' in el) {
      // get position
      pos = el.selectionStart;
      // ELSE IF a range is selected
    } else if ('selection' in document) {
      // focus on element
      el.focus();
      // create the range
      const Sel = document.selection.createRange();
      // get length
      const SelLength = document.selection.createRange().text.length;
      // get start position
      Sel.moveStart('character', -el.value.length);
      // get end position
      pos = Sel.text.length - SelLength;
    }
    // return position
    return pos;
  },

  /**
      @method V.mod.element.selectRange
      @description Select a Range within an input
      @param {string} start  Start position of range
      @param {string} end End position of range
      @example V.mod.element.selectRange(0,12)
   */
  selectRange(start, end) {
    // If end is not specified handle as a selection / cursor placement
    if (!end) end = start;
    // Select the range from the dom with cross browser checks for supported range selection methods
    return this.each(function () {
      if (this.setSelectionRange) {
        this.focus();
        this.setSelectionRange(start, end);
      } else if (this.createTextRange) {
        const range = this.createTextRange();
        range.collapse(true);
        range.moveEnd('character', end);
        range.moveStart('character', start);
        range.select();
      }
    });
  },

  /**
     @method V.mod.element.exists
     @description Check whether an element exists on the DOM
     @param {string} element
     @example  V.mod.element.exists('#example-div-id')
   */
  exists(element) {
    return ($(element).length > 0) ? true : false;
  },

  /**
     @method V.mod.element.removeEmpty
     @description Remove empty elements from the DOM
     @param {string} element
     @example V.mod.element.removeEmpty('.elements-to-check')
   */
  removeEmpty(element) {

    // Remove empty elements
    $(element).each(function () {
      if ($(this).text() === "") {
        $(this).remove();
      }
    });
  },

  /**
     @method V.mod.element.text
     @description check for the existance of a text string
     @example V.mod.element.text('Hello World')
     @returns {boolean} true/false 
   */
  text(searchText) {
    return ($(`div:contains("${searchText}")`).length > 0) ? true : false;
  },


  /**
     @method V.mod.element.equalizeHeight
     @description Equalise the heights of elements
     @param {string} element
     @example  V.mod.element.equalizeHeight('.css-class')
   */
  equalizeHeight(element) {
    let maxHeight = 0;
    $(element).each(function () {
      if ($(this).height() > maxHeight) {
        maxHeight = $(this).height();
      }
    });
    $(element).height(maxHeight);
  },

  /**
     @method V.mod.element.equalizeBox
     @description Equalise the height to the width of an element / group
     @param {string} element
     @example  V.mod.element.equalizeBox('.tile')
   */
  equalizeBox(element) {
    $(element).each(function () {
      if ($(this).height() !== $(this).width()) {
        $(this).height($(this).width());
      }
    });
  },

  /**
     @method V.mod.element.getOffsetTop
     @description Get offset from top
     @param {string} element
     @example  V.mod.element.getOffsetTop(el)
   */
  getOffsetTop(elem) {
    let location = 0;
    if (elem.offsetParent) {
      do {
        location += elem.offsetTop;
        elem = elem.offsetParent;
      } while (elem);
    }
    return location >= 0 ? location : 0;
  },

  /**
     @method V.mod.element.getHeight
     @description Get height of element
     @param {string} element
     @example  V.mod.element.getHeight(el)
   */
  getHeight(elem) {
    if (typeof element === 'string') elem = $(elem)[0];
    return Math.max(elem.scrollHeight, elem.offsetHeight, elem.clientHeight);
  }
};