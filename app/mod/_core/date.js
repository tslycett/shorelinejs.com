/*
     @module V.mod.date
     @description Date helpers
     @summary Date helpers
 */

V.mod.date = {

  /**
    @method V.mod.date.stamp
    @description get a unix-like timestamp
    @returns {string} date stamp
    @example  V.mod.date.stamp()
  */
  stamp() {
    return Math.floor(new Date() / 1000);
  },

  /**
     @method V.mod.date.process
     @description returns a plain date from a timestamp
     @param {string} date
     @returns {string} formatted date
   */
  process(date) {
    let tmp = new Date(parseInt(date));
    tmp = tmp.toString().split('GMT');
    return tmp[0]
  },

  /**
     @method V.mod.date.diffNow
     @description Returns the difference of date to the current date
     @param {date} date Date value to process
     @example V.mod.date.diffNow(date/value from/now)
     @returns {number} number of days from now
   */
  diffNow(date) {
    // Returns the difference of date to the current date
    const prevTime = new Date(date);
    // now
    const thisTime = new Date();
    // now - past
    const diff = thisTime.getTime() - prevTime.getTime();
    // positive number of days
    const result = diff / (1000 * 60 * 60 * 24);
    // round off
    const rounded = Number(`${Math.round(`${result}e${0}`)}e-${0}`);
    // return result
    return `${rounded} days ago`;
  },

  /**
    @method V.mod.date.diffToNow
    @description Returns the difference of date to the current date
    @param {date} date Date value
    @example V.mod.date.diffToNow(date/valuefrom/now)
    @returns {date} rounded difference of date to the current date  (days till)
   */
  diffToNow(date) {
    // Returns the difference of date to the current date  (days till)
    const future = new Date(date);
    // now
    const thisTime = new Date();
    // future - now
    const diff = future.getTime() - thisTime.getTime();
    // positive number of days
    const result = diff / (1000 * 60 * 60 * 24);
    // round it off
    const rounded = Number(`${Math.floor(`${result}e${0}`)}e-${0}`);
    // return result
    return rounded;
  },

  /**
    @method V.mod.date.diffToNowPos
    @description Returns the difference of date to the current date with day included   (ie days left)
    @param {date} date Date value to process
    @returns {date} difference of date to the current date
   */
  diffToNowPos(date) {
    // get current date
    const future = new Date(date);
    // now
    const thisTime = new Date();
    // future - now
    const diff = future.getTime() - thisTime.getTime();
    // positive number of days
    const result = diff / (1000 * 60 * 60 * 24);
    // round it off
    const rounded = Number(`${Math.floor(`${result}e${0}`)}e-${0}`);
    // return result
    return rounded;
  },

  /**
    @method V.mod.date.future
    @description Get a Date in the future by specifying the amount of days to add
    @param {number} daysToAdd number of days to add from today
    @example V.mod.date.future(12)
    @returns {date} futureDate
   */
  future(daysToAdd) {
    // get date
    const today = new Date();
    // create the future date
    const futureDate = new Date(today.getFullYear(), today.getMonth(), today.getDate() + parseInt(daysToAdd));
    // return result
    return futureDate;
  },

  /**
     @method V.mod.date.maxValue
     @description Get the largest value from a dataset
     @param {array} data a dataset to process
     @example V.mod.date.maxValue(['data', 'array'])
     @returns {string} Largest value
   */
  maxValue(data) {
    // set a base var
    let max = '';
    // loop over array
    for (let i = 0; i < data.length; i++) {
      // IF value is larger
      if (data[i] > max) {
        // set max to value
        max = data[i];
      }
      // return result
      return max;
    }
  },

  /**
     @method V.mod.date.minValue
     @description Get the closest date to today from an array
     @param {array} DateArray Array of JS date values
     @example V.mod.date.minValue(['date/value/1', 'date/value/2','date/value/3'])
     @returns {date} Smallest Date
   */
  minValue(DateArray) {
    // set a base value from the array
    let SmallestDate = new Date(DateArray[0]);
    // iterate over array and compare
    for (let i = 1; i < DateArray.length; i++) {
      // assign to date
      const TempDate = new Date(DateArray[i]);
      // IF if its smaller than index date
      if (TempDate < SmallestDate) {
        // re-assign smallest value
        SmallestDate = TempDate;
      }
    }
    // return result
    return SmallestDate;
  },

  /**
     @method V.mod.date.sqlToJsDate
     @description Quickly convert a MySQL Date Time string into a JavaScript date object
     @param {string} datetime
        MySQL Date Time string, Split timestamp into [ Y, M, D, h, m, s ]
     @example V.mod.date.sqlToJsDate('msql/Date/String/..')
     @returns {string} formatted JS date
   */
  sqlToJsDate(datetime) {
    // Split timestamp into [ Y, M, D, h, m, s ]
    const t = datetime.split(/[- :]/);
    // return formatted date
    return new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
  },

  /**
     @method V.mod.date.iso
     @description ISO formatted timestamp
     @returns  {string} ie. 2017-01-18T11:58:32.977+02:00
   */
  iso() {
    const now = new Date();
    const tzo = -now.getTimezoneOffset();
    const dif = tzo >= 0 ? '+' : '-';
    const pad = num => {
      const norm = Math.abs(Math.floor(num));
      return (norm < 10 ? '0' : '') + norm;
    };
    return `${now.getFullYear()}-${pad(now.getMonth() + 1)}-${pad(now.getDate())}T${pad(now.getHours())}:${pad(now.getMinutes())}:${pad(now.getSeconds())}.${pad(now.getMilliseconds())}${dif}${pad(tzo / 60)}:${pad(tzo % 60)}`;
  }
};
