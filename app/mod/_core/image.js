/*
   Image helpers
   @module V.mod.img
*/
V.mod.img = {

  /**
    @method V.mod.img.preload
    @description Preload images to speed up rendering
    @param {array} arr Array of image urls
    @example  V.mod.img.preload(['img1', 'img2'])
    */
  preload(arr) {
    if (!$.preloadImages) {
      $.preloadImages = (arr) => {
        $.each(arr, () => {
          $("<img />").attr("src", arr[i]);
        });
      };
    }
    $.preloadImages(arr);
  },

  /**
    @method V.mod.img.replace
    @description Replace broken images on error
    @param {string} img img to attach listener to
    @param {string} replacementImage img to replace with
    @example  V.mod.img.replace('img.jpg', 'img2.jpg')
    */
  replace(img, replacementImage) {
    $(img).error(function () {
      $(this).attr(src, replacementImage);
    });
  },

  /**
    @method V.mod.img.hide
    @description Hide broken images completely
    @example V.mod.img.hide()
  */
  hide() {
    $("img").error(function () {
      $(this).hide();
      V.log(`[V.mod.img.hide] : ${this}`);
    });
  },

  /**
    @method V.mod.img.base64
    @description Base64 encode an image
    @param {string} img image to encode
    @returns {string} Bae64 encoded image
    @example V.mod.img.base64('//www.img.com/img.jpg')
  */
  base64(img) {
    const canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    const copyImg = canvas.getContext("2d");
    copyImg.drawImage(img, 0, 0);
    const imgUrl = canvas.toDataURL("image/png");
    return imgUrl.replace(/^data:image\/(png|jpg);base64,/, "");
  },

  /**
    @method V.mod.img.cache
    @description store Base64 encoded images to localStorage
    @param {string} selectors array of images or image selectors (ie '#img-to-cache')
    @example V.mod.img.cache('img')
  */
  cache(imgArr) {
    if (typeof imgArr === 'string') {
      imgArr = $(imgArr);
    }
    if (imgArr.length > 0) {
      $.each(imgArr, (k, v) => {
        V.db.local.store(v, V.mod.img.base64(v));
      });
    }
  },

  /**
    @method V.mod.img.loadCached
    @description restore Base64 encoded images in localStorage to DOM
    @example V.mod.img.loadCached()
  */
  loadCached() {
    $.each($('img'), (k, v) => {
      if (V.db.local.get(v.src) !== null) {
        v.src = `data:image/png;base64,${V.db.local.get(v.src)}`;
      }
    });
  }
};