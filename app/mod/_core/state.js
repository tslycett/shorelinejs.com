/*
 * @method V._state
 * @description current Browser State model
 */
V._state = {

  url: '',

  history: [],

  events: {},

  model: {},

  components: {},

  store() {
    // Save latest state to local storage
    V.db.local.set('view', JSON.stringify($('view').html() ));
  },

  load() {
    // Load saved state from local storage on page load
    $('view').html(JSON.parse(V.db.local.get('view')));
  },

  clear() {
    this.components = {};
    this.model = {};
  }
};
