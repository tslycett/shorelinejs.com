
/**
  @method V.route
  @description Create a in browser url path and controller
  @param {string} url Url Path to assign view to
  @copyright page.js
  @see https://github.com/visionmedia/page.js
  @example
      V.route('/user', function(){ // do stuff })
      V.route('/user/:user', function(){ // do stuff })

      all params / props are passed to the init function 
      or available on V._state.model
  @example
      page('#!/home')   or   page('/home')  to navigate to a route
*/


// route creator 
V.route = (url, controller) => {
  page(url, controller);
  // page(url, V.route.animationOut, controller, V.route.animationIn);
};

// Animation options
V.route.animationIn = () => { };
V.route.animationOut = () => { };

// default auto-unwatch
V.route._unwatch = () => { };


/**
 * @method V.route.go
 * @param {string} url url to route to
 * @example V.route.go('/home')
 */
V.route.go = (url) => {
  // hash routes used by default
  if (V._config.hash && V._config.hash === false){  page(url) } else { page(`#!/${url}`) }
}

/**
 * @method V.route.exec
 * @description replace this function for adding a function/callback to route system
 * @example V.route.exec = () => { do something when route triggers };
 */
V.route.exec = () => { };


  // Configure page.js
  page.base(V._config.baseRoute || '/');
  page('/*', () => { page(V._config.defaultRoute || '/') });

V.route.init = () => {

  // init Controllers
  $.each(V.view, (k) => {

    // run bootscripts
    if (V.view[k] && V.view[k].boot) { V.view[k].boot() }

    // create pubsub events for models
    if (V.view[k].subscribe) { V.event.on(V.view[k].subscribe, (data) => { $.extend(V.view[k].model, data, true) }) }

    // autocreate routes
    if (V.view[k] && V.view[k].route) {

      // Create route and intialize
      V.route(V.view[k].route, (ctx) => {

        // destroy previous watchers
        V.route._unwatch();

        // load all assets on this route
        V.req.assets(V.view[k]);

        // watch conditional rendering
        if (V.view[k].watch && V.view[k].watch === true && V.view[k].model && V.view[k].render) {
          watch(V.view[k].model, () => { V.view[k].render() }, 10, true);
        }

        // set unwatch for page unlload
        V.route._unwatch = () => { unwatch(V.view[k].model) }

        // store history and clear state
        V._state.url = document.location;
        V._state.history.push(document.location.href);
        V._state.model = ctx || {};
        V._state.components = [];
        V._state.events = {};

        // initialize route
        V.view[k].init(ctx);

        // scroll browser to top of view
        $(window).scrollTop(-1);
        
        // run appended function
        V.route.exec();

        // Google analytics handlers
        if (V.mod.analytics && V.mod.analytics._config.enabled === true && ga) { ga('send', 'pageview', document.location.href) }
      });
    }
  });

  // intiialise and configure page.js
  page({
    click: false,
    popstate: true,
    dispatch: false,
    hashbang: V._config.hash || true,
    decodeURLComponents: true
  });
}
