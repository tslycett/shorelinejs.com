/*jshint -W030 */

/*
    @global V ShorelineJS
    @description global V app object
    @author github/timlycett
    @version 0.1
*/


var V = {

  // component storage object
  com: {},

  // data storage space
  data: {},

  // Module storage object
  mod: {},

  // view / controller storage space
  view: {},

  // sockets and server communication layer
  _io: window.io || false,

  /*
      @method V._init
      @description Start the App - This runs on document load
      @example V._init()
  */
  _init() {
    $.each(V.mod, (k) => {
      if (V.mod[k].init) { V.mod[k].init() }
      if (V.mod[k]['_config'] && V._config.mod[k]) { $.extend(V.mod[k]['_config'], V._config.mod[k], true) }
    });
    V.req.assets(V._config);
    V._config.init();
    V.route.init();
  }
};

// initialize on document load
$(document).ready(() => { V._init() });
