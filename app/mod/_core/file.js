/*
  @module V.mod.file
*/

V.mod.file = {

  /**
    @method V.mod.file.download
    @description create a downloadable file in browser
    {@link http://www.framework7.com| View full docs}
    @param {string} anchorSelector link to attach download to
    @param {string} str body of file
    @param {string} fileName name of file
    @example
        Example dom element
                '<a id="export" class="myButton" download="" href="#">Download</a>'

        Example initialisation:
                var str = "hi,file";
                V.mod.download("#export", str, "file.txt");
 
  */
  download(anchorSelector, str, fileName) {

    if (window.navigator.msSaveOrOpenBlob) {
      const fileData = [str];
      blobObject = new Blob(fileData);
      $(anchorSelector).click(() => {
        window.navigator.msSaveOrOpenBlob(blobObject, fileName);
      });
    } else {
      const url = `data:text/plain;charset=utf-8,${encodeURIComponent(str)}`;
      $(anchorSelector).attr("download", fileName);
      $(anchorSelector).attr("href", url);
    }
  },


  /**
     @method V.mod.file.gdoc
     @description Open a file in Google Docs
     {@link https://www.google.com/docs/about/| Learn more about Google Docs}
     @param  {string} selector selector of link to attach download
     @param  {string} file file url
     @param  {object} options extended options (optional)
     @example V.mod.file.gdoc('#id-of-link', 'text in file')
   */
  gdoc(selector, file, options) {

    let settings = {
      width: '100%',
      height: '100%'
    };
    if (options) { $.extend(settings, options)} 

    const ext = file.substring(file.lastIndexOf('.') + 1);
    if (/^(tiff|pdf|ppt|pps|doc|docx)$/.test(ext)) {
      $(selector).append(`<div id="gdoc-${selector}" class=""><iframe src="http://docs.google.com/viewer?embedded=true&url=${encodeURIComponent(file)}" width="${settings.width}" height="${settings.height}" style="border: none;" seamless></iframe></div>`);
    }
  }
}

