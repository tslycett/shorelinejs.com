/*
     Google Analytics Module
     @version 0.1
 */

V.mod.analytics = {

  _config: {
    enabled: false,
    clientId: false
  },

  /**
       @method V.mod.analytics.ajaxListener
       @description Listens for AJAX errors and reports it to google service
       @example V.mod.analytics.ajaxListener();
   */
  ajaxListener() {
    $(document).ajaxError((e, request, settings) => {
      _gaq.push(['_trackEvent', 'Ajax error', settings.url, e.result, true]);
    });
  },

  /**
       @method V.mod.analytics.disable
       @description Disable a google analytics script in the global scope
       @example V.mod.analytics.disable('ua-12345645')
   */
  disable(trackingCode) {
    window[`ga-disable-${trackingCode}`] = true;
  },

  /**
       @method V.mod.analytics.errorlistener
       @description Listens for JS errors and reports to google service
       @example V.mod.analytics.errorListener()
   */
  errorlistener() {
    window.addEventListener('error', e => {
      _gaq.push(['_trackEvent', 'JavaScript Error', e.message, `${e.filename}:  ${e.lineno}`, true]);
    });
  },

  /**
       @method V.mod.analytics.track
       @description Create a temporary global _ga object and loads google-analytics library to DOM
       @param {string} trackingCode The google analytics tracking code to initialise to
       @example V.mod.analytics.track('ua-12345645')
   */
  track(trackingCode) {

    (((i, s, o, g, r, a, m) => {

      // Acts as a pointer to support renaming.
      i.GoogleAnalyticsObject = r;

      // Creates an initial ga() function.  The queued commands will be executed once analytics.js loads.
      i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments);
      };

      // Sets the time (as an integer) this tag was executed.  Used for timing hits.
      i[r].l = 1 * new Date();

      // Insert the script tag asynchronously.  Inserts above current tag to prevent blocking in
      // addition to using the async attribute.
      a = s.createElement(o);
      m = s.getElementsByTagName(o)[0];
      a.async = 1;
      a.src = g;
      m.parentNode.insertBefore(a, m);

    }))(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    // Creates the tracker with default parameters.
    ga('create', trackingCode, 'auto', {
      'cookieName': '_ga2'
    });

    // Sends a pageview hit and alerts library loaded for dev.
    ga('send', 'pageview', document.location.href);
  },

  /**
     @method V.mod.analytics.view
     @description Manually send a google analytics view event for SPA's 
     @example V.mod.analytics.view()
   */
  view() {
    ga('send', 'pageview', () => {
      V.log('INFO :: [V.mod.analytics] :: ga sent');
    });
  }
};
