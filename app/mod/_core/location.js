/*
 * @module V.mod.location
 * @description GPS and location helpers
 */
V.mod.location = {

  locate() {
    navigator.geolocation.getCurrentPosition((position) => {
      $('[name=lat]').val(position.coords.latitude);
      $('[name=lng]').val(position.coords.longitude);
    }, error => { console.log(error) });
  },

  distance(lat1, lon1) {
    navigator.geolocation.getCurrentPosition((position) => {
      function toRad(Value) {
        return Value * Math.PI / 180;
      }
      let lat2 = position.coords.latitude;
      const lon2 = position.coords.longitude;
      const R = 6371;
      const dLat = toRad(lat2 - lat1);
      const dLon = toRad(lon2 - lon1);
      var lat1 = toRad(lat1);
      lat2 = toRad(lat2);
      const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
      const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
      const d = R * c;
      return d;
    }, err => {
      if (err) { V.log(err) }
    });
  },

  iplookup() {
    $.get("https://ipinfo.io", response => response, "jsonp");
  }
};

