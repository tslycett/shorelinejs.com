/**
   @module V.req
   @description Request handler (works like ajax but also supports socket.io websockets
   @param  {object} obj request object
   @example
    
    V.req({
    url: '/api',
    type: 'get',           // get or post
    headers: {},           // set headers
    data: {},              // for post requests
    success(data, headers) {},
    error(err) {}
  });
 */

V.req = obj => {

  let options = {
    url: false,
    type: 'get',
    headers: false,
    data: {},
    success() {
      V.log(`${options.url} :: completed request`);
    },
    error() {
      V.log(`ERROR :: [V.req] ::  failed url: ${options.url}`);
    }
  };
  $.extend(options, obj, true);

  // // send via sockets or normal ajax if no socket connection is present
  if (V._io && V._io.socket && V._io.socket.isConnected() === true) {

    // set headers
    if (options.headers !== false) { V._io.socket.headers = options.headers }

    // Make req
    V._io.socket[options.type](options.url, options.data, (response, jwtHeaders) => {
      options.success(response || jwtHeaders);
    });

  } else if ($.ajax) {

    if (options.url !== false) {
      // set headers
      if (options.headers !== false) { V._io.socket.headers = options.headers }

      // options.dataType = "json";
      // options.contentType = "application/json; charset=utf-8";
      $.ajax(options);

    } else {
      V.log("ERROR :: [V.req] :: no url defined in ajax request");
    }
  } else {
    V.log('warn :: [V.req] :: no request handler / premature req')
  }
};




/**
   @method V.req.assets
   @desc load assets in a obj path
   @param  {object} obj Object to check for 'scripts' / 'styles' arrays to load
   @example V.req.assets(V.view.home)
 */
V.req.assets = (obj, cb) => {

  if (obj.scripts) {
    $.each(obj.scripts, (k2, v2) => {
      if ($(`[src="${v2}"]`).length < 1) {
        $('head').append(`<script src="${v2}" async></script>`);
      }
    });
  }

  if (obj.styles) {
    $.each(obj.styles, (k3, v3) => {
      if ($(`[href="${v3}"]`).length < 1) {
        $('head').append(`<link rel="stylesheet" href="${v3}" />`);
      }
    });
  }

  (cb && typeof cb === "function") ? cb() : '';
};