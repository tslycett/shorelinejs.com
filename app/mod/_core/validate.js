/*
  @summary Extends the view
  @version 0.1
  @author github/timlycett
  @module validate
*/
V.mod.validate = {

  pattern: {
    email: /\S+@\S+\.\S+/,
    phone: /((\(\d{3}\)?)|(\d{3}))([\s-./]?)(\d{3})([\s-./]?)(\d{4})/,
    zip: /^\d{5}(?:[-\s]+\d{4})?$/,
    url: /^(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/,
    userid: /^[A-Za-z0-9_]{3,20}$/,
    username: /^[a-z0-9_-]{3,16}$/,
    password: {
      length: /^[A-Za-z0-9!@#$%^&*()_]{6,20}$/,
      strong: /^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*W).*$/,
      medium: /^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$/,
      weak: /(?=.{6,}).*/
    },
    url2: "/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[.\!\/\\w]*))?)/ig",
    email2: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/g,
    htmlTags: "/<\/?[a-z][a-z0-9]*[^<>]*>|<!--.*?-->/img",
    deviceInfo: "/(?<ModelCode>\w{1})\((?<CountryCode>\w{3,4})\)\-(?<ModelNumber>\w{2})(\-(?<StackerType>\w{2}))? (?<InterfaceType>ID003\-\d{2})v(?<SoftwareVersion>\d{3}\-\d{2}) (?<SotfwareDate>\w+) (?<Crc>\w{4})/i",
    number: "/^[+-]?(\pN+($|(?P>float)?e-?\pN+$)|(?<float>\pN*[,.]\pN+))",
    ifElse: "/if\s*\((.+?)\)\s*{(.*?)}\s*?(else\s*?if\s*?\((.+?)\)\s*?{.*?})?\s*?(else\s*?{(.*?)})?/",
    extractStyle: "/\<style\>(.*)\<\/style\>/s",
    hexColor: /^#?([a-f0-9]{6}|[a-f0-9]{3})$/,
    slug: /^[a-z0-9-]+$/,
    ip: /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/,
    specialChar: /[^\x00-\x7f]/
  },

  /**
      @method V.mod.validate.password
      @description Validate a password and check strength
      @param {string} element Element to validate
      @param {string} validatorElement Element to send user feedback to
      @example V.mod.validate.password('#input', '#label')
   */
  password(element, validatorElement) {

    const strongRegex = V.mod.validate.pattern.password.strong;
    const mediumRegex = V.mod.validate.pattern.password.medium;
    const enoughRegex = V.mod.validate.pattern.password.length;
    let msg;

    if (false === enoughRegex.test($(element).val())) {
      msg = 'More characters';
    } else if (strongRegex.test($(element).val())) {
      msg = 'Strong password';
    } else if (mediumRegex.test($(element).val())) {
      msg = 'OK ( medium strength )';
    } else {
      msg = 'password too weak!';
    }
    $(validatorElement).html(msg);
  },

  rule: {}, // namespace validation storage

  rules(selector, value) {

    // clean up passed selector
    const name = selector.replace('#', '');
    // check whther validation exists for the field
    const obj = (V.mod.validate.rule[name]) ? V.mod.validate.rule[name] : V.log(`INFO :: [V.mod.validate.rules] :: no UI validation rules for ID: ${selector}`);

    // If validation
    if ($.isArray(obj)) {

      let passed = [];
      let failed = [];

      // test all patterns
      $.each(obj, (k, v) => {
        (v.pattern.test(value) === true) ? passed.push(k) : failed.push(k);
      });

      V.log(`passed : ${passed}: failed : ${failed}`);

      if (failed.length === 0) {
        // it passed all tests and gets the passed class
        $(`#${name}`).css('border-bottom', '3px solid rgba(1, 255, 1, 0.3)');
        // clear error box text
        $(`#error-${name}`).text('');

      } else {
        // add error class
        $(`#${name}`).css('border-bottom', '3px solid rgba(255, 1, 1, 0.3)');
        // save lookups by storing obj in a single variable
        const err_el = $(`#error-${name}`);
        // clear the error box
        err_el.text('');

        // send messages to error-box
        $.each(failed, (k, v) => {
          err_el.append(obj[k]['msg'] + ((failed.length > 1 && k !== failed.length - 1) ? ' & ' : ''));
        });
      }

    } else if (typeof obj == 'object') {
      V.mod.validate.valid(selector, value, obj.pattern, obj.msg);
    }
  },

  /**
    @method V.mod.validate.valid
    @description Validate a RegEx pattern and assign CSS classes
    @param {string} id ID of element to check
    @param {string} pattern Regex pattern to validate against
    @param {string} message Validation message to user
    @example
        V.mod.validate.valid("asd", /asd/g, "not valid");
        // Assigns a 'has-error'  or  'has-passed' css class to the element
  */
  valid(id, value, pattern, message, argz) {

    const el = $(`#${id.replace('#', '')}`);
    const logic = (argz) ? argz : true;

    if (pattern.test(value) === logic) {
      el.css('border-bottom', '3px solid rgba(1, 255, 1, 0.3)');
      $(`#error-${id}`).text('');

    } else {
      el.css('border-bottom', '3px solid rgba(255, 1, 1, 0.3)');
      $(`#error-${id}`).text(message);
    }
  }
};
