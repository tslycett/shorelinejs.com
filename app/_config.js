/*
 * @name ShorelineJS
 * @description Browser SPA framework
 * 
 * MASTER CONFIG
 * This file serves to allow you to configure some base features and hold you specific global data for templating
 */
V._config = {

	// Base site config params - these are available in tmeplates as ie. {{@global.cdn}}
	site: {
		name: "app",
		env: "development",
		host: document.location.origin + "/",
		cdn: document.location.origin + "/",
	},

	// -set the router's default page 
	// defaultRoute: '/',

	// -use hashbangs     NB- server must support your routes if false
	//	hash: false,

	// -$() selector of default element to render to (App container)
	//element: '#view',

	// -default layout to use
	// layout: `<view id="view"></view>`,


	// loads scripts on document ready (lazy loader)
	scripts: [],

	// -loads stylesheets on document ready  (use <head> instead for essential css)
	styles: [],

	// -Module configs
	mod: { analytics: { enabled: false, track: "", clientid: "" }	},

	init() {

		// Start your app here  ;)





	}
};
