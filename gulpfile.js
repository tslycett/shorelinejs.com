/* =========================== CREDITS / ABOUT  ========================================
    @description:  master gulpfile
    @author: github/timlycett
    - free for re-use / private abuse / copy ;)


    ShorelineJS Build File   
======================================== PATHS ======================================== */

let paths = {

  project: {
    name: 'app',
    version: '0.0.1'
  },

  globals: ['$', 'window', 'document', 'V', 'Template7', 'WatchJS', 'watch'],
  
  scripts: [
    'app/assets/js/vendor/zepto.js',
    'app/assets/js/vendor/page.js',
    'app/assets/js/vendor/template7.min.js',
    'app/assets/js/vendor/watch.js',
    // Add your base JS files here

  ],

  css: [
    // add your base css files here

    'app/components/*/*.css',
    'app/views/*/*.css',
    'app/assets/styles/app.css',
    'app/assets/styles/font/fonts.css'
  ],

  view: [
    // Core libs (required)
    'app/mod/_core/view.js',
    'app/_config.js',
    'app/mod/_core/log.js',
    'app/mod/_core/db.js',    
    'app/mod/_core/data.js',
    'app/mod/_core/route.js',
    'app/mod/_core/event.js',
    'app/mod/_core/fn.js',
    'app/mod/_core/req.js',
    'app/mod/_core/render.js',
    'app/mod/_core/state.js',

    // Mods (optional)
    'app/mod/_core/file.js',
    'app/mod/_core/browser.js',
    'app/mod/_core/element.js',
    'app/mod/_core/image.js',
    'app/mod/_core/secure.js',
    'app/mod/_core/date.js',
    // 'app/mod/_core/analytics.js',

    // file includes
    'app/views/*/*.js',
    'app/components/*/*.js',
    'app/assets/js/templates.js'
  ],


/* ************************** RATHER DO NOT EDIT BELOW THIS POINT  ******************************* */

  npm: [
    'app/mod/_core/view.js',
    'app/_config.js',
    'app/mod/_core/log.js',
    'app/mod/_core/db.js',    
    'app/mod/_core/data.js',
    'app/mod/_core/route.js',
    'app/mod/_core/event.js',
    'app/mod/_core/fn.js',
    'app/mod/_core/req.js',
    'app/mod/_core/render.js',
    'app/mod/_core/file.js',
    'app/mod/_core/browser.js',
    'app/mod/_core/element.js',
    'app/mod/_core/image.js',
    'app/mod/_core/secure.js',
    'app/mod/_core/date.js',
    'app/mod/_core/analytics.js',
    'app/mod/_core/state.js',
    'app/mod/_core/debug.js',
    'app/mod/_core/require.js'
  ],
  standalone: [
    'app/assets/js/vendor/zepto.js',
    'app/assets/js/vendor/page.js',
    'app/assets/js/vendor/template7.min.js',
    'app/assets/js/vendor/watch.js',
    'app/mod/_core/view.js',
    'app/_config.js',
    'app/mod/_core/log.js',
    'app/mod/_core/db.js',    
    'app/mod/_core/data.js',
    'app/mod/_core/route.js',
    'app/mod/_core/event.js',
    'app/mod/_core/fn.js',
    'app/mod/_core/req.js',
    'app/mod/_core/render.js',
    'app/mod/_core/file.js',
    'app/mod/_core/browser.js',
    'app/mod/_core/element.js',
    'app/mod/_core/secure.js',
    'app/mod/_core/date.js',
    'app/mod/_core/image.js',
    'app/mod/_core/state.js'
  ],
  lite: [
    'app/assets/js/vendor/template7.min.js',
    'app/assets/js/vendor/watch.js',
    'app/assets/js/vendor/page.js',
    'app/mod/_core/view.js',
    'app/_config.js',
    'app/mod/_core/log.js',    
    'app/mod/_core/db.js',
    'app/mod/_core/data.js',
    'app/mod/_core/route.js',
    'app/mod/_core/event.js',
    'app/mod/_core/fn.js',
    'app/mod/_core/req.js',
    'app/mod/_core/render.js',
    'app/mod/_core/browser.js',
    'app/mod/_core/state.js'
  ],
  less: [
    'app/assets/styles/less/variables.less',
    'app/assets/styles/less/main.less',
    'app/components/*/*.less',
    'app/views/*/*.less',
  ],
  images: [
    'app/assets/images/*.png',
    'app/assets/images/*.jpg',
    'app/assets/images/*.jpeg',
    'app/assets/images/*.gif',
    'app/assets/images/*.svg'
  ],
  fonts: 'app/assets/styles/font/**',
  icons: 'app/assets/icon/**',
  videos: 'app/assets/videos/**',
  extras: ['app/assets/robots.txt', 'app/assets/favicon.ico'],
  css_compiled: 'app/assets/styles/styles.css',
  templates: ['app/views/*/*.html', 'app/components/*/*.html'],
  js_compiled: 'app/assets/js/V.dev.js',
  devfolder: 'app/',
  copy: 'app/assets/js/vendor/**',
  dist: {
    app: 'app/assets/',
    web: 'dist/production/',
  }
};






/* ======================================== MODULE inits ======================================== */

gulp = require('gulp');
gulp = require('gulp-help')(require('gulp'));
const fs = require('fs');
const server = require('gulp-server-livereload');
const util = require('gulp-util');
const rename = require('gulp-rename');
const jshint = require('gulp-jshint');
const eslint = require('gulp-eslint');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const clean = require('gulp-clean');
const connect = require('gulp-connect');
const cssmin = require('gulp-cssnano');
const csslint = require('gulp-csslint');
const htmlhint = require('gulp-htmlhint');
const autoprefixer = require('gulp-autoprefixer');
const cssnext = require('gulp-cssnext');
const sourcemaps = require('gulp-sourcemaps');
const babel = require('gulp-babel');
const htmlmin = require('gulp-minify-html');
const combineMedia = require('gulp-combine-media-queries');
const size = require('gulp-size');
const stripDebug = require('gulp-strip-debug');
const shell = require('gulp-shell');
const plumber = require('gulp-plumber');
const wrapper = require('gulp-wrapper');
const documentation = require('gulp-documentation');
const imagemin = require('gulp-imagemin');
const plato = require('plato');
const open = require("open");
const replace = require('gulp-replace');
const html2js = require('gulp-html2js');
const less = require('gulp-less');
const lesshint = require('gulp-lesshint');
const zip = require('gulp-zip');


/* ======================================== Combined TASKS ======================================== */

// Test all DEV/ APP resources for errors
gulp.task('test', 'run debuggers', ['lint', 'lint-html']);


gulp.task('b', 'quick build view & launch livereload server', shell.task([
  'gulp templates',
  'gulp view',
  'gulp less',
  'gulp css',
  'gulp'
]));

gulp.task('build', 'build production app', shell.task([
  'gulp templates',
  'gulp scripts',
  'gulp view',
  'gulp images',
  'gulp copy',
  'gulp less',
  'gulp css'
]));

gulp.task('push', 'git add, commit, push', shell.task([
  // 'gulp lint',
  // 'gulp test',
  'echo build done',
  'git add .',
  'echo files added to git',
  'git commit -am "commited : ' + new Date() + '"',
  'echo files commited for git',
  'git push',
  'echo done - pushed'
]));


// install sub folder dependencies
gulp.task('startup', 'run first time installs', shell.task([
  'gulp help',
  'gulp doc',
  'gulp build',
  'gulp default'
]));

// install sub folder dependencies
gulp.task('doc', 'run doc generators', shell.task([
  'gulp clean-doc',
  'gulp doc-view',
  'gulp doc-app'
]));


/* ======================================== JS ======================================== */

/*
 *  SCRIPTS
 *  This will DEBUG the code, combine them, and 'uglify' them to one
 *  file called 'all.min.js' in your 'dist' folder
 */

// compiled
gulp.task('scripts', 'Build dependencies', (done) => {

  console.log('SCRIPTS run');

  return gulp.src(paths.scripts)
    .pipe(size())
    .pipe(stripDebug())
    .pipe(concat(paths.project.name + '-vendor.js'))
    .pipe(uglify())
    .pipe(size())
    .pipe(gulp.dest(paths.dist.app + 'js/'))
    .pipe(gulp.dest(paths.dist.web + 'js/'));

  done();
});

// VIEW
gulp.task('view', false, (done) => {

  console.log('VIEW run');

  // Production build
  gulp.src(paths.view)
    .pipe(size())
    .pipe(babel({
      compact: true,
      comments: false,
      minified: true
    }))
    .pipe(replace(/('|")use strict\1/g, ''))
    .pipe(concat(paths.project.name + '-view.js'))
    .pipe(sourcemaps.write('.'))
    .pipe(sourcemaps.init())
    .pipe(size())
    .pipe(gulp.dest(paths.dist.web + 'js/'));

  // Dev build
  gulp.src(paths.view)
    .pipe(babel({
      compact: false
    }))
    .pipe(replace(/('|")use strict\1/g, ''))
    .pipe(concat(paths.project.name + '-view.dev.js'))
    .pipe(gulp.dest(paths.dist.app + 'js/'));

  done();
});


gulp.task('standalone', 'Build a lite zepto standalone of ShorelineJS', (done) => {

  console.log('ShorelineJS standalone run');

  // Production build
  gulp.src(paths.standalone)
    .pipe(size())
    .pipe(stripDebug())
    .pipe(babel({
      compact: true,
      comments: false,
      minified: true
    }))
    .pipe(replace(/('|")use strict\1/g, ''))
    .pipe(concat('ShorelineJS.zepto.js'))
    .pipe(sourcemaps.write('.'))
    .pipe(sourcemaps.init())
    .pipe(size())
    .pipe(gulp.dest('dist/standalone/'))
    .pipe(zip('ShorelineJS.zepto.zip'))
    .pipe(size())
    .pipe(gulp.dest('dist/standalone/'));


  //
  gulp.src(paths.standalone)
    .pipe(documentation("md"))
    .pipe(gulp.dest('dist/standalone/'));

  gulp.src(paths.standalone)
    .pipe(documentation("html"))
    .pipe(gulp.dest('dist/standalone/doc/'));
  done();
});






gulp.task('lite', 'Build a lite drop-in version of ShorelineJS', (done) => {

  console.log('ShorelineJS lite run');

  // Production build
  gulp.src(paths.lite)
    .pipe(size())
    .pipe(stripDebug())
    .pipe(babel({
      compact: true,
      comments: false,
      minified: true
    }))
    .pipe(replace(/('|")use strict\1/g, ''))
    .pipe(concat('ShorelineJS.lite.js'))
    .pipe(sourcemaps.write('.'))
    .pipe(sourcemaps.init())
    .pipe(size())
    .pipe(gulp.dest('dist/lite/'))
    .pipe(zip('ShorelineJS-lite.zip'))
    .pipe(size())
    .pipe(gulp.dest('dist/lite/'));


  //
  gulp.src(paths.lite)
    .pipe(documentation("md"))
    .pipe(gulp.dest('dist/lite/'));

  gulp.src(paths.lite)
    .pipe(documentation("html"))
    .pipe(gulp.dest('dist/lite/docs/'));

  done();
});






/* ======================================== CSS ======================================== */

/*
 *  CSS task
 *  combines css and the runs 'uncss' to remove unneeded css classes. It then minifies and
 *  packs css into a styles.css file
 */

// dist
gulp.task('css', false, (done) => {

  console.log('CSS run');

  return gulp.src(paths.css)
    .pipe(plumber())
    .pipe(cssmin())
    .pipe(concat('styles.css'))
    .pipe(cssnext())
    .pipe(gulp.dest(paths.dist.app + '/'))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(size())
    .pipe(gulp.dest(paths.dist.web + '/styles/'));
  done();
});



gulp.task('less', false, (done) => {
  console.log('LESS run');

  gulp.src(paths.less)
    .pipe(plumber())
    .pipe(concat('styles.less'))
    .pipe(less())
    .pipe(rename(paths.project.name + '-styles.css'))
    .pipe(cssmin())
    .pipe(cssnext())
    .pipe(size())
    .pipe(gulp.dest(paths.dist.app + '/styles/'));

  done();
});

/* ======================================== BUILD TASKS ======================================== */

gulp.task('images', false, (done) => {
  console.log('IMAGES run');

  return gulp.src(paths.images)
    // .pipe(imagemin({
    //   // JPG Lossless conversion to progressive. (def: false)
    //   progressive: true,
    //   // GIF: Interlace gif for progressive rendering. (def: false)
    //   interlaced: true,
    //   // SVG: Optimize svg multiple times until it's fully optimized.  (def: false)
    //   multipass: true,
    //   // PNG: Select an optimization level between 0 and 7.   (def: 3)
    //   optimizationLevel: 3
    // }))
    .pipe(gulp.dest(paths.dist.web + 'images/'));

  done();
});


/*
 *  COPY task
 *  moves files across to 'dist'
 */

gulp.task('copy', false, (done) => {
  console.log('COPY run');

  // Fonts
  gulp.src(paths.fonts)
    .pipe(gulp.dest(paths.dist.web + 'styles/font/'));

  // Icons
  gulp.src(paths.icons)
    .pipe(gulp.dest(paths.dist.web + 'icon/'));

  // videos
  gulp.src(paths.videos)
    .pipe(gulp.dest(paths.dist.web + 'videos/'));

  done();
});


/*
 *   HTML-Template task
 *   Builds a combined template file
 */

gulp.task('templates', false, (done) => {
  console.log('TEMPLATES run');

  let opts = {
    //do not remove conditional internet explorer comments
    conditionals: true,
    // do not remove redundant attributes
    spare: true,
    // do not strip CDATA from scripts
    cdata: false,
    // do not remove comments
    comments: false,
    // do not remove arbitrary quotes
    quotes: true,
    // preserve one whitespace
    loose: false
  };

  gulp.src(paths.templates)
    .pipe(size())
    .pipe(htmlmin(opts))
    .pipe(size())
    .pipe(html2js('templates.js', {
      adapter: 'javascript',
      base: 'app/',
      name: 'module',
      useStrict: true
    }))
    .pipe(wrapper({
      header: '(function() {',
      footer: '\n V.templates = module; module = {}; })();'
    }))
    .pipe(gulp.dest(paths.dist.app + '/js/'));

  done();
});




// WIP - requires jsdom for build and export
gulp.task('npm', false, (done) => {
  console.log('NPM run');

  gulp.src(paths.npm)
    .pipe(size())
    .pipe(concat('index.js'))
    .pipe(wrapper({
      header: `(function() {`,
      footer: '\n exports = V })();'
    }))
    .pipe(gulp.dest('dist/npm/'));
  done();
});



/* ======================================== Project ZIP TASKS ======================================== */


gulp.task('save', 'save & zip a project', () => {
  gulp.src(['app/**'])
    .pipe(zip(`${paths.project.name}-${Date()}.zip`))
    .pipe(gulp.dest('apps/' + paths.project.name));
});


gulp.task('cleanall', false, (done) => {
  gulp.src([
    `dist/production`,
    `dist/documentation`,
    `./app/assets/js/${paths.project.name}-vendor.js`,
    `./app/assets/js/${paths.project.name}-view.dev.js`,
    `./app/assets/js/templates.js`,
    `./app/assets/styles/${paths.project.name}-styles.css`,
    `./app/components/*/**`,
    `./app/views/*/**`
  ])
    .pipe(size())
    .pipe(clean());

  done();
});

// run a buncha tasks
gulp.task('runall', false, shell.task([
  'gulp build',  
  'gulp standalone',
  'gulp lite',
  'gulp doc',
  'gulp gen-view --name home',
  'gulp gen-com --name nav',
  'gulp b'
]));



/* ======================================== DEBUGGING / HELPERS ======================================== */


/*
 *  DEBUG task
 *  This runs various tests to debug the code during development
 */

gulp.task('lint', 'debug JS', () => {

  return gulp.src(paths.view)
    .pipe(babel())
    .pipe(eslint({
      fix: true,
      env: {
        "es5": true,
        "es6": true,
        "es7": true
      },
      parser: "babel-eslint",
      rules: {
        'no-alert': 1,
        'no-bitwise': 0,
        'camelcase': 1,
        'curly': 1,
        'eqeqeq': 1,
        'no-eq-null': 1,
        'guard-for-in': 1,
        'no-empty': 1,
        'no-use-before-define': 1,
        'no-obj-calls': 2,
        'no-unused-vars': 1,
        'new-cap': 1,
        'no-shadow': 0,
        'strict': 1,
        'no-invalid-regexp': 1,
        'comma-dangle': 1,
        'no-undef': 0,
        'no-new': 1,
        'no-extra-semi': 1,
        'no-debugger': 2,
        'no-caller': 1,
        'semi': 1,
        'quotes': 1,
        'no-unreachable': 2
      },
      globals: paths.globals,
      envs: ['browser']
    }))
    .pipe(eslint.format());
});


/************* HTML *************/

/*
 *   HTML-HINT task
 *   verifies, checks and debugs html
 */

gulp.task('lint-html', false, (done) => {
  return gulp.src(paths.templates)
    .pipe(htmlhint({
      "doctype-first": false
    }))
    .pipe(htmlhint.reporter());

  done();
});

/************* JS *************/


/*
 *  DEBUG tests task
 *  This runs various tests to debug the test code during development
 */

gulp.task('lint-tests', false, (done) => {
  return gulp.src(paths.test.specs)
    .pipe(jshint())
    .pipe(jshint.reporter('default'));

  done();
});

/************* CSS *************/

/*
 *  CSSLINT task
 *  Debugs and checks css files for errors
 */

const customReporter = (file) => {
  gutil.log(gutil.colors.cyan(file.csslint.errorCount) + ' errors in ' +
    gutil.colors.magenta(file.path));

  file.csslint.results.forEach((result) => {
    gutil.log(result.error.message + ' on line ' + result.error.line);
  });
};

// This allows configuration of the csslint rules for testing
// Notably the postprocessing tools need to be considered and hence certain rules can be ommited
// like [ie vendor-prefixes - as autoprefixer is post-run]
// It also allows you to granularly solve one problem at a time by enabling your current concern only ++

gulp.task('lint-css', false, (done) => {
  return gulp.src(paths.css)
    .pipe(csslint({
      'adjoining-classes': true,
      'box-model': true,
      'box-sizing': true,
      'compatible-vendor-prefixes': false,
      'empty-rules': true,
      'display-property-groupings': true,
      'display-background-images': true,
      'duplicate-properties': true,
      'fallback-colors': true,
      'floats': true,
      'font-faces': true,
      'font-sizes': true,
      'gradients': true,
      'ids': true,
      'import': true,
      'important': true,
      'known-properties': true,
      'outline-none': true,
      'overqualified-elements': true,
      'qualified-headings': true,
      'regex-selectors': true,
      'shorthand': true,
      'star-property-hack': true,
      'text-indent': true,
      'underscore-property-hack': true,
      'unique-headings': true,
      'universal-selector': true,
      'unqualified-attributes': true,
      'vendor-prefix': false,
      'zero-units': true
    }))
    .pipe(csslint.formatter());
  done();
});




gulp.task('lint-less', false, () => {
  return gulp.src(paths.less)
    .pipe(lesshint({
      // Options
    }))
    .pipe(lesshint.reporter()) // Leave empty to use the default, "stylish"
    .pipe(lesshint.failOnError()); // Use this to fail the task on lint errors
});


/* ======================================== DOCUMENTATION TASKS ======================================== */

gulp.task('doc-view', false, (done) => {

  // Plato
  plato.inspect(paths.view, 'dist/documentation/ShorelineJS/code', {
    title: 'ShorelineJS Documentation',
    eslint: {
      parser: 'babel-eslint',
      esversion: 7
    }
  }, (report) => { open('dist/documentation/ShorelineJS/code/index.html')});

  // JSDOC 
  gulp.src(paths.view)
    .pipe(documentation("md"))
    .pipe(gulp.dest('dist/documentation/ShorelineJS/docs'));

  gulp.src(paths.view)
    .pipe(documentation("html", {}, {
      version: '0.0.1',
      name: 'ShorelineJS'
    }))
    .pipe(gulp.dest('dist/documentation/ShorelineJS/docs'))
    .pipe(open('dist/documentation/ShorelineJS/docs/index.html'));

  done();
});


gulp.task('doc-mods', false, (done) => {
  gulp.src('app/mod/_core')
    .pipe(documentation("md", {}, {
      version: '0.0.1',
      name: 'ShorelineJS'
    })).pipe(gulp.dest('dist/documentation/mods'));
  done();
});


gulp.task('doc-app', false, (done) => {

  // todo - build only modules that are being used and run doc gen off that

  let files = [
    'app/views/*/*.js',
    'app/components/*/*.js',
    'app/_config.js'
  ];

  // Renders out annotations in standard jsDoc format via 'documentation' plugin
  gulp.src(files)
    .pipe(documentation("md"))
    .pipe(gulp.dest('dist/documentation/app/docs'));
  // render out plato analysis code files
  let outputDir = 'dist/documentation/app/code';
  let callback = (report) => { };
  let options = {
    title: 'app Documentation',
    eslint: {
      parser: "babel-eslint",
      esversion: 7
    }
  };
  plato.inspect(files, outputDir, options, callback);
  done();
});


// DOCS
gulp.task('clean-doc', false, (done) => {
  return gulp.src('./dist/documentation')
    .pipe(size())
    .pipe(clean());
  done();
});



/* ======================================== LAUNCH TASKS ======================================== */

gulp.task('default', function () {

  gulp.watch(['app/views/*/*.js', 'app/components/*/*.js', 'app/mod/_core/*.js', 'app/views/_config.js'], ['view']);
  gulp.watch(['app/views/*/*.css', 'app/components/*/*.css', 'app/assets/styles/*.css'], ['css']);
  gulp.watch(['app/views/*/*.less', 'app/components/*/*.less', 'app/assets/styles/less/*.less'], ['less']);
  gulp.watch(['app/views/*/*.html', 'app/components/*/*.html'], ['templates', 'view']);

  gulp.src('app')
    .pipe(server({
      livereload: true,
      directoryListing: false,
      open: true,
      port: 9000
    }));
});

/* ======================================== CLI Generators ======================================== */


gulp.task('gen-view', 'generate a VIEW --name foo', () => {

  // pass name in [--key param]   ie.     gulp gen-view --name thisIsAworkingName
  let name = util.env.name || 'todo';

  let content = `
V.view.${name} = {
  
  // boot() {},
  // subscribe:[],
  // scripts:[],
  // styles: [],
  // watch : true,

    route: '${name}',

    init() {  this.render() },
    
    model: {},

    render() {
    V.render({
        data: this.model,
        template: V.templates['views/${name}/${name}.html'],
        // callback() {}
    });
  },
}`;

  if (!fs.existsSync(`app/views/${name}`)) {
    console.log(`The folder  app/views/${name} was succesfully made!`);
    fs.mkdirSync(`app/views/${name}`);
  }

  fs.writeFile(`app/views/${name}/${name}.js`, content, "utf8", (err) => {
    if (err) throw err;
    console.log(`The controller ${name}.js was succesfully written!`);
  });

  fs.writeFile(`app/views/${name}/${name}.html`, `<h1>${name}</h1>`, "utf8", (err) => {
    if (err) throw err;
    console.log(`The template ${name}.html was succesfully written!`);
  });

  fs.writeFile(`app/views/${name}/${name}.less`, `.${name} {}`, "utf8", (err) => {
      if (err) throw err;
      console.log(`The less / css file  ${name}.less was succesfully written!`);
    });
});







gulp.task('gen-com', 'generate a COMPONENT   --name foo', () => {
  // pass name in [--key param]   ie.     gulp gen-com --name thisIsAworkingName
  let name = util.env.name || 'todo';

  let content = `
V.com.${name} = {

    model: {
    class: ".${name}",
    body: 'component ${name}'
    },

    render(el) {
      V.render({
        el: el,
        data: this.model,
        template: V.templates['components/${name}/${name}.html']
      });
  }
}`;

  if (!fs.existsSync(`app/components/${name}`)) {
    console.log(`The folder  app/components/${name} was succesfully made!`);
    fs.mkdirSync(`app/components/${name}`);
  }

  fs.writeFile(`app/components/${name}/${name}.js`, content, "utf8", (err) => {
    if (err) throw err;
    console.log(`The component ${name}.js was succesfully written!`);
  });

  fs.writeFile(`app/components/${name}/${name}.html`, `<div id="{{id}}" class="{{class}}">{{body}}</div>`, "utf8", (err) => {
    if (err) throw err;
    console.log(`The template ${name}.html was succesfully written!`);
  });

  fs.writeFile(`app/components/${name}/${name}.less`, `.${name} {}`, "utf8", (err) => {
      if (err) throw err;
      console.log(`The less / css file  ${name}.less was succesfully written!`);
    });
});