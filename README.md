

![./app/assets/ShorelineJS.svg](./app/assets/ShorelineJS.svg)



# ShorelineJS 

###### ShorelineJS is a small Single App framework that is easily extendible and works as a jquery add-on, standalone drop-in or a full SPA with build system. The basis of the system is to allow you to focus on the views, components, style and templates in a close to native way using modern tooling that fills in the gaps and helps you along the way...



### Built in features include -

> Flexible & extendible App model

> {{}} syntax - see http://idangero.us/template7 for options and info.

> Template helpers with mod & global access

> Layouts & Component Injection

> express-like router with query params (hash/history)

> pubsub & event system

> browser db - local / session / cookie

> element, browser, object, array, string, date ++ helpers

> watch models - https://github.com/melanke/Watch.JS/

> automagic initializers ++


 ### optional mod extras

> debuggers, google-analytics, validation ++


# Getting Started


#### Basic rendering example


```

<script src="..."></script>

<div id="view"></div>

<script>

V.render({
  el: '#view',
  data: { example: 'hello world' },
  template: '<h1>{{example}}</h1>'
});

</script>

```


## Render options

```javascript

V.render({
  el: '',             // The $() selector to use, or else <body> is the default
  layout: '',         // optional layout to use for the render
  template: '',       // can be compiled templates, dom templates or JS strings with handlebars power ;)
  api: '',            // Api is linked to the REQ mod, and will request the data before rendering ($.ajax / socket.io)
  filter(){},         // filter data received from API before rendering   (useful for api above)
  data: {},           // Data object to use for rendering when not using api
  prerender(){},      // Compile template and manipulate rendered string before it is rendered to the dom
  callback(){}        // The callback for tasks after rendering
});

```


## VIEW options:

```javascript

  V.view.example = {                    // namespace your view 
    route: 'example',                   // creates a route. calls init() function on navigation to route
    scripts: [],                        // scripts are autoloaded on navigation if not already on DOM
    styles: [],                         // css files are autoloaded on navigation if not already on DOM
    boot() {},                          // boot function is run on system boot-up (script load)
    init() {},                          // called on navigation to this route
    subscribe: ['example'],             // subscribe to pubsub events of this name and update model
    watch: true,                        // this will automatically do  watch(this.model, () => { this.render() })  on boot
    model: {},                          // define your model
    render() {}                         // render function  - called when using   watch:true
  };
  
```



# Lite edition

#### drop in add-on for upgrading Jquery systems (v1.9 +)

> Ideal add-on for Jquery systems & webpages with a small footprint and modern accessible living model.

> This will allow modern templating, routing and rendering on your already established or brand new system.

> It should enduce reduced DRYer modern code.

> for more methods and capabilities see https://github.com/timlycett/shorelineJS/tree/master/dist/lite/API.md 




# Standalone edition

> Includes Lite features, more modules & http://zeptojs.com. No dependencies.

> for more methods and capabilities see https://github.com/timlycett/shorelineJS/tree/master/dist/standalone/API.md






# Single Page App System

Complete build system with Babel / ES6 support. 

GET STARTED:

```
clone this repo

npm i -g gulp

cd ShorelineJS && npm install

gulp startup

```



# Build System

ShorelineJS comes with its own customisable gulp build system, generators and CLI.

```

 - gulp help - for all CLI options

 - gulp b - quick build and run live reload server

 - gulp-build - to build production files

```

The build system also has:

- live-reload
- debuggers and linters
- auto-imports & compiles LESS/SCSS
- minifies and converts HTML templates to JS
- JS and Babel ES6
- builds, compresses, polyfills, prefixes, optimises and compiles
- customisable build paths


# CLI GENERATORS

## generating a VIEW

```
gulp gen-view --name home
```
Open code in your IDE of choice, and check in ```app/views/home/``` for your new View. 



## generating a COMPONENT

```
gulp gen-com --name navbar
```

Check in ```app/components/navbar``` for your new Component.



# Basic view

```javascript

  V.view.home = {
    route: 'home',                    // creates browser route @   #!/home   or   /home if server routes can abide
    init() { this.render() },         // called on router navigation to this page
    render() {
      V.render({
        el: 'body',
        data: {ie: 'hello world'},
        template: '<h1>{{ie}}</h1>'
      });
    }
  }

```



# Model bound view (auto-renders when model changes)

```javascript

  V.view.home = {
    route: 'home',
    init() { this.render() },
    watch: true,
    model: {},
    render() {
      V.render({
        data: this.model,
        template: V.templates['views/home/home.html']
      });
    }
  };

```



# Events

### using native DOM events

> onclick="V.com.sidebar('x')"

> onchange="V.event.emit('x.y.z', this.value)"

see https://www.quirksmode.org/dom/events/ for a full list of native dom events


Native events on the DOM are preferable if there is truly cross-browser support. This in a SPA sense lets the browser handle event cancellations when dom items are removed. 

### document bubbles 

> V.event(id, cb)

> V.data(id, namespace)


### PubSub

>  V.event.on(name, cb)

>  V.event.emit(name, data);


# Documentation

ShorelineJS comes with a comprehensive documentation generator:

- JSDOC to html/MD docs

- PLATO analysis code docs - maintenance and debugging overview.

> Documentation will be generated when you run ```gulp startup``` or ```gulp doc```
